import math

class GlyphTable:
    @classmethod
    def build(cls, glyphs):
        glyph_count = len(glyphs)
        columns = int(math.sqrt(glyph_count))

        pos = 0
        res = []

        while pos < glyph_count:
            res.append(glyphs[pos:pos+columns])
            pos += columns

        return cls(res, columns)


    def __init__(self, glyph_table: list[list[str]], columns):
        self._table = glyph_table
        self._columns = columns

    @property
    def table(self) -> list[list[str]]:
        return self._table

    @property
    def rows(self) -> int:
        return len(self.table)

    @property
    def columns(self) -> int:
        return self._columns

    def get(self, x: int, y: int) -> str:
        return self.table[y][x]

    def find(self, glyph: str) -> tuple[int, int] | None:
        for y in range(self.rows):
            for x in range(len(self.table[y])):
                if self.get(x, y) == glyph:
                    return (x, y)

        return None

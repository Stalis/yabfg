#!/usr/bin/env python3
import sys
import yabfg.font_map_generator as font_map_generator

NAMES_PREFIX = 'FONT_'
SOURCE_START = '''// Managed by Stalis Font Generator. Do not edit
#pragma once

#ifdef __cplusplus
extern "C" {
#endif
'''
SOURCE_END = '''
#ifdef __cplusplus
}
#endif
'''
ARR_TYPE = 'uint16_t'
ARR_NAME = 'LETTERS'
GLYPHS_SET_NAME = 'GLYPHS'

def generate_get_letter_func(glyps_set_name: str = GLYPHS_SET_NAME,
                                              arr_name: str = ARR_NAME,
                                              arr_type: str = ARR_TYPE,
                                              prefix: str = NAMES_PREFIX,
                                              function_name: str = 'get_letter') -> list[str]:
    lines = []

    lines.append(f'const {arr_type}* {prefix}{function_name}(char ch) {{')
    lines.append(f'    const char* ch_ptr = strchr({prefix}{glyps_set_name}, ch);')
    lines.append(f'    const size_t index = ch_ptr - {prefix}{glyps_set_name};')

    lines.append(f'    return {prefix}{arr_name}[index];')
    lines.append('}')

    return lines


def generate_letters_array(glyphs_order: str, glyphs: dict[str, list[tuple[int, int]]],
                        arr_name: str = ARR_NAME,
                        arr_type: str = ARR_TYPE,
                        prefix: str = NAMES_PREFIX) -> list[str]:
    lines = []
    glyphs_count = len(glyphs_order)
    max_arr_len = max([ len(points) for points in glyphs.values() ]) * 2 + 1
    lines.append(f'const {arr_type} {prefix}{arr_name}[{glyphs_count}][{max_arr_len}] = {{')

    for glyph in glyphs_order:
        glyph_points_len = len(glyphs[glyph])
        glyph_points = ', '.join([ f'{point[0]}, {point[1]}' for point in glyphs[glyph] ])

        lines.append(f'    {{  // {glyph} ')
        lines.append(f'        {glyph_points_len},')
        lines.append(f'        {glyph_points}')
        lines.append('    },')

    lines.append('};')
    return lines



def generate_source(glyphs: dict[str, list[tuple[int, int]]],
                    glyphs_set_name: str = GLYPHS_SET_NAME,
                    prefix: str = NAMES_PREFIX,
                    arr_name: str = ARR_NAME,
                    arr_type: str = ARR_TYPE) -> list[str]:
    source = []
    source.append(SOURCE_START)

    chars = ''.join(glyphs.keys())
    source.append(f'const char* {prefix}{glyphs_set_name} = "{chars}";')
    source.append('')

    source.extend(generate_letters_array(chars, glyphs, prefix=prefix, arr_name=arr_name, arr_type=arr_type))
    source.append('')

    source.extend(generate_get_letter_func(glyps_set_name=glyphs_set_name,
                                                        arr_name=arr_name,
                                                        arr_type=arr_type,
                                                        prefix=prefix))
    source.append(SOURCE_END)
    return source

if __name__ == '__main__':
    generator = font_map_generator.FontGenerator(sys.argv[1])
    glyphs = generator.get_glyph_pixels()

    print('\n'.join(generate_source(glyphs)))

#!/usr/bin/env python3
import sys
from .glyph_table import GlyphTable
from PIL import Image, ImageFont, ImageDraw
from .font_map import FontMap

GLYPHS = 'ABCDEFGHIJKLMNOPQRTUVWXYZ' +\
         'abcdefghijklmnopqrstuvwxyz' +\
         '0123456789 _!@#$%^&*()'


class FontMapGenerator:
    def __init__(self,  font_path: str,
                        glyphs: str = GLYPHS,
                        glyph_height: int = 16,
                        glyph_width: int = 8,
                        glyph_h_spacing: int = 0,
                        glyph_v_spacing: int = 0,
                        font_size: int = 16):
        self.font_path = font_path
        self.glyphs = glyphs
        self.glyph_table = GlyphTable.build(self.glyphs)
        self.glyph_height = glyph_height
        self.glyph_width = glyph_width
        self.glyph_h_spacing = glyph_h_spacing
        self.glyph_v_spacing = glyph_v_spacing
        self.font_size = font_size

    @property
    def img_width(self) -> int:
        return self.glyph_h_spacing + self.glyph_table.columns * (self.glyph_width +
                                                        self.glyph_h_spacing)

    @property
    def img_height(self) -> int:
        return self.glyph_v_spacing + self.glyph_table.rows * (self.glyph_height +
                                                    self.glyph_v_spacing)

    def get_glyph_box(self, glyph: str) -> tuple[int, int, int, int] | None:
        coords = self.glyph_table.find(glyph)
        if coords is None:
            return None

        x, y = coords
        x0 = self.glyph_h_spacing + x * (self.glyph_width + self.glyph_h_spacing)
        y0 = self.glyph_v_spacing + y * (self.glyph_height + self.glyph_v_spacing)
        x1 = x0 + self.glyph_width
        y1 = y0 + self.glyph_height

        return (x0, y0, x1, y1)

    def get_glyphs_boxes(self) -> dict[str, tuple[int, int, int, int]]:
        res = {}
        for glyph in self.glyphs:
            res[glyph] = self.get_glyph_box(glyph)

        return res

    def render_font(self) -> FontMap:
        img = Image.new('1', (self.img_width, self.img_height))
        draw = ImageDraw.Draw(img)

        font = ImageFont.truetype(self.font_path, size=self.font_size)

        for glyph in self.glyphs:
            box = self.get_glyph_box(glyph)
            draw.text((box[0], box[1]), glyph, font=font, fill=1)

        img = img.crop((0, 0, self.img_width, self.img_height))
        return FontMap(img, self.get_glyphs_boxes())


if __name__ == '__main__':
    generator = FontMapGenerator(sys.argv[1], glyphs=GLYPHS)
    font_map = generator.render_font()

    print(font_map.glyph_pixels['A'])
    font_map.img.save('font.png')

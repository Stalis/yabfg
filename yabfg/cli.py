#!/usr/bin/env python3
import click
from .font_map_generator import FontMapGenerator
from .generators.c_header import generate_source

@click.group()
def main():
    pass

@main.command()
@click.argument('generator')
@click.argument('font')
def gen(generator: str, font: str):
    click.echo(f'Generating by {generator}...')
    gen = FontMapGenerator(font)
    font_map = gen.render_font()
    lines = generate_source(font_map.glyph_pixels)
    print('\n'.join(lines))


if __name__ == '__main__':
    main()

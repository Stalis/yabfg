#!/usr/bin/env python3
from PIL import Image


class FontMap:
    def __init__(self, img: Image.Image,
                       glyph_boxes: dict[str, tuple[int, int, int, int]]):
        self.img = img
        self.glyph_boxes = glyph_boxes

    @property
    def glyph_pixels(self) -> dict[str, list[tuple[int, int]]]:
        if self.img is None:
            self.render_font()

        res = {}
        for glyph in self.glyph_boxes:
            res[glyph] = self.get_glyph_pixels(glyph)

        return res

    def get_glyph_pixels(self, glyph) -> list[tuple[int,int]]:
        box = self.glyph_boxes[glyph]
        pixels = []

        for x in range(box[0], box[2]):
            for y in range(box[1], box[3]):
                if self.img.getpixel((x, y)) > 0:
                    pixels.append((x, y))

        return pixels

import pytest
from glyph_table import GlyphTable

@pytest.mark.parametrize('glyphs,cols,rows', [
    ('ABab', 2, 2),
    ('ABCab', 2, 3)
])
def test_build_dimensions(glyphs, cols, rows):
    table = GlyphTable.build(glyphs)
    assert table.columns == cols
    assert table.rows == rows


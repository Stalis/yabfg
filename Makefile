.PHONY: venv test setup

venv:
	python3 -m venv --prompt '[> yabfg <]' .venv

test:
	PYTHONPATH=./yabfg pytest

setup:
	pip install --editable .

from setuptools import setup, find_packages

setup(
    name='yabfg',
    version='0.1.0',
    python_requires='>=3.8',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3'
    ],
    packages=find_packages(where='yabfg'),
    package_dir={'yabfg': 'yabfg', 'generators': 'yabfg/generators'},
    include_package_data=True,
    tests_require=[
        'pytest'
    ],
    install_requires=[
        'Click',
    ],
    entry_points={
        'console_scripts': [
            'yabfg = yabfg.cli:main',
        ],
    },
)

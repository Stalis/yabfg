#pragma once

const char* FONT_GLYPHS = "ABCabc123"

const uint16_t FONT_LETTERS[9][] = {
    {
        1, 0, 2
    },
    {

    }
};

uint16_t[] FONT_get_letter(char ch) {
    const char* ch_ptr = strnchr(FONT_GLYPHS, 9, ch);
    const size_t index = ch_ptr - FONT_GLYPHS;

    return FONT_LETTERS[index];
}
